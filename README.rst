

Flask-Kit
====================


Simple utilite to configure flask app with popular extensions:
Database with Flask-SQLAlchemy, API with Flask-RESTful,
and migrations with Flask-Migrate

Also provide some module autoloading from 'sources' folder, to register
custom business logic modules

